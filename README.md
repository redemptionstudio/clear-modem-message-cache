# Modem Cache Manager

This is alpha level software. Many things are missing and are not guaranteed to work. You use this software at your own risk. This software is missing standard variable validation.

Modem Cache Manager was originally written to address issues with the PinePhone modem's cache filling up due to MMS messages being unable to be processed, but this turned out to be a red herring. The script has been rewritten to address a new modem cache issue due to Chatty, or MMSD-TNG, not properly removing read messages from the PinePhone modem's cache. The 0.0.5 rewrite can be used to set up a cron job that alerts the user when the modem's cache is filling up and it can be used to clear read messages, rather than the original's ability to clear all messages.

## Usage

```
  Options:
    -h   --help                 Show this message
    -D   --debug                Enables debug mode. Currently unimplemented.
    -v   --version              Show TITLE, VERSION, AUTHOR, and DATE
    -V   --verbose              Turn on verbose mode
    -c   --config               Include a config file. Currently unimplemented.
    -t   --get-message-limit    Report modem's message capacity.
    -a   --alert                Check if modem's capacity is greater than 50%, 80%, 90%, or 100%.
    -r   --clear                Clear the modem's cache of read messages.
```


#!/bin/bash

# Clear Modem Message Cache  Copyright (C) 2020  T. H. Wright               
# This program comes with ABSOLUTELY NO WARRANTY; for details use `-h'.
# This is free software, and you are welcome to redistribute it             
# under certain conditions; see COPYING for details.                      

################################
# Information
TITLE="Clear Modem Message Cache"
VERSION="0.0.5"
DATE="20211202"
AUTHOR="T. H. Wright"
LICENSE="GPLv3"
DESC="The PinePhone's modem cache fills due to read messages not getting deleted. This script sends alerts and clears read messages."
#
################################

usage() {
cat << EOF
GPLv3 License information: 15. Disclaimer of Warranty.

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

$TITLE, v. $VERSION, by $AUTHOR
Last modified: $DATE

$DESC

  Usage: script <[options]>
  Options:
    -h   --help			Show this message
    -D   --debug		Enables debug mode. Currently unimplemented.
    -v   --version		Show TITLE, VERSION, AUTHOR, and DATE
    -V   --verbose		Turn on verbose mode
    -c   --config		Include a config file. Currently unimplemented.
    -t   --get-message-limit	Report modem's message capacity.
    -a   --alert		Check if modem's capacity is greater than 50%, 80%, 90%, or 100%.
    -r   --clear		Clear the modem's cache of read messages.
	
EOF

}

# BEGIN DEFINE VARIABLES                                         
DEBUG=false;                                                     
VERBOSE=false;
MODEMDEVICE='/dev/EG25.AT'
MODEMNUMBER=$(sudo mmcli -L | awk '{print $1}' | awk -F'/' '{print $6}')
oIFS=$IFS                                                        
IFS=$'\n'                                                        
                                                                
CACHEDSMS=""
MESSAGECOUNTER=0
# END DEFINE VARIABLES

# BEGIN DEFINE FUNCTIONS

verbose() {
	[[ $VERBOSE = true ]] && return 0 || return 1
}

getCachedMessages () {
	verbose && echo "Getting cached message list.";
	echo 'AT+CMGL=4' | sudo atinout - $MODEMDEVICE - > /tmp/atcachedmessages;
	verbose && echo "Cached message list stored in /tmp/atcachedmessages.";
}

getReadMessages () {
	verbose && echo "Getting read message list."
	getCachedMessages;
	
	verbose && echo "Creating temporary file without message data."
	sudo grep '+CMGL' /tmp/atcachedmessages > /tmp/unprocessedatmessages;

	verbose && echo "Removing old pull file.";
	sudo rm /tmp/atreadmessages;
	touch /tmp/atreadmessages;

	verbose && echo "Pruning unread messages from list."
	for CACHEDMSG in $(cat /tmp/unprocessedatmessages); do
		MSGNUMBER=$(echo "$CACHEDMSG" | awk '{print $2}' | awk -F ',' '{print $1}');
		READSTATUS=$(echo "$CACHEDMSG" | awk '{print $2}' | awk -F ',' '{print $2}');
		if [ "$READSTATUS" == '1' ]; then
			verbose && echo "Adding Message # $MSGNUMBER to list."
			echo $CACHEDMSG >> /tmp/atreadmessages
		fi
	done
	verbose && echo "Removing temporary file without message data.";
	sudo rm /tmp/unprocessedatmessages;

	verbose && echo "Read message list stored in /tmp/atreadmessages."
}

getTotalMessages () {
	getReadMessages;
	TOTALMSG=$(echo 'AT+CPMS?' | sudo atinout - $MODEMDEVICE - | grep '+CPMS' | awk '{print $2}' | awk -F',' '{print $2}');
	READMSG=$(cat /tmp/atreadmessages | wc -l);
	MSGLIMIT=$(echo 'AT+CPMS?' | sudo atinout - $MODEMDEVICE - | grep '+CPMS' | awk '{print $2}' | awk -F',' '{print $3}')

	echo "Total Messages in Modem: $TOTALMSG. Number of Read Messages: $READMSG. Modem has a message limit of $MSGLIMIT."
}

cacheAlert () {
	getTotalMessages;
	FIFTY=$( expr $MSGLIMIT / 2 )
	EIGHTY=$( expr $MSGLIMIT * 0.75 )
	NINETY=$( expr $MSGLIMIT * 0.9 )
	if [ "$TOTALMSG" -gt "$FIFTY" ]; then
		echo "Cached messages greater than 50% capacity."
	elif [ "$TOTALMSG" -gt "$EIGHTY" ]; then
		"Cached messages greater than 80% capacity."
	elif [ "$TOTALMSG" -gt "$NINETY" ]; then
		"Cached messages greater than 90% capacity."
	elif [ "$TOTALMSG" == "$MSGLIMIT" ]; then
		echo "Modem cache full."
	else
		echo "Cached messages are fewer than half the modem's capacity."
	fi
}

clearCachedReadMessages () {
	getReadMessages;
	
	verbose && echo "Removing cached messages from modem.";
	for CACHEDMSG in $(cat /tmp/atreadmessages); do
		MSGNUMBER=$(echo "$CACHEDMSG" | awk '{print $2}' | awk -F ',' '{print $2}');
		verbose && echo "Removing Message # $MSGNUMBER from modem.";
		echo "AT+CMGD=$MSGNUMBER" | sudo atinout - $MODEMDEVICE -;
	done
	
	sudo m /tmp/atcachedmessages;

	verbose && echo "Cached modem messages removed.";
}

# BEGIN OPTARGS
reset=true
for arg in "$@"
do
    if [ -n "$reset" ]; then
      unset reset
      set -- 
    fi
    case "$arg" in
	--help)    set -- "$@" -h ;;
	--version) set -- "$@" -V ;;	
	--verbose) set -- "$@" -v ;;
	--debug)   set -- "$@" -D ;;
	--config)  set -- "$@" -c ;;
	--get-message-limit) set -- "$@" -t ;;
	--alert)   set -- "$@" -a ;;
	--clear)   set -- "$@" -r ;;
	*)         set -- "$@" "$arg" ;;
    esac
done

while getopts ":hVvDc:tar" opt; do
    case $opt in
        h)  usage && exit ;;
	V)  echo "$TITLE, v. $VERSION by $AUTHOR. $DATE." && exit;;
        v)  VERBOSE=true ;;
	D)  DEBUG=true; echo "Debug mode enabled." ;;
        c)  source $OPTARG ;;
	t)  getTotalMessages >&2 && exit ;;
	a)  cacheAlert >&2 && exit ;;
	r)  clearCachedReadMessages >&2 && exit ;;
        \?) echo "$TITLE: -$OPTARG: undefined option." >&2 && usage >&2 && exit ;;
        :)  echo "$TITLE: -$OPTARG: missing argument." >&2 && usage >&2 && exit ;;
    esac
done
shift $((OPTIND-1))

# END OPTARGS
